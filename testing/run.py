from pathlib import Path
import sys
if str(Path(__file__).resolve().parent.parent) not in sys.path:
    sys.path.append(str(Path(__file__).resolve().parent.parent))
from src.vtofg.funcgen import FunctionGenerator

gen = FunctionGenerator(plot=True)
gen.mainloop()